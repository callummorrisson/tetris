(function() {
	let Engine = Matter.Engine,
        Render = Matter.Render,
        Runner = Matter.Runner,
        Composite = Matter.Composite,
        Composites = Matter.Composites,
        Common = Matter.Common,
        Events = Matter.Events,
        MouseConstraint = Matter.MouseConstraint,
        Mouse = Matter.Mouse,
        World = Matter.World,
        Vector = Matter.Vector,
        Vertices = Matter.Vertices,
        Bounds = Matter.Bounds,
        Body = Matter.Body,
        Bodies = Matter.Bodies,
        Query = Matter.Query;

    let windowWidth = window.innerWidth,
        windowHeight = window.innerHeight,
        blockThickness = windowHeight / 20,
        boundSize = blockThickness / 10,
        velocityModifier = blockThickness / 20,
        gameWidth = blockThickness * 8.5, 
        gameHeight = windowHeight,
        gameLeft = (windowWidth - gameWidth) / 2,
        gameTop = 0,
        gameBottom = gameTop + gameHeight,
        gameRight = gameLeft + gameWidth,
        wallColor = 'rgba(255,255,255,0.2)',
        wallThickness = blockThickness;

        let lineDef = [0,0,1,0,1,4,0,4].map(x => x * blockThickness),
        teeDef = [0,0,3,0,3,1,2,1,2,2,1,2,1,1,0,1].map(x => x * blockThickness),
        leftLDef = [0,0,2,0,2,3,1,3,1,1,0,1].map(x => x * blockThickness),
        rightLDef = [0,0,2,0,2,1,1,1,1,3,0,3].map(x => x * blockThickness),
        leftSDef = [1,0,2,0,2,2,1,2,1,3,0,3,0,1,1,1].map(x => x * blockThickness),
        rightSDef = [0,0,1,0,1,1,2,1,2,3,1,3,1,2,0,2].map(x => x * blockThickness),
        squareDef = [0,0,2,0,2,2,0,2].map(x => x * blockThickness);

    let line = { 
            vertices: Vertices.fromPath(lineDef.join(" ")),
            color: 'rgba(255,255,255,0.8)'
        }, 
        tee = { 
            vertices: Vertices.fromPath(teeDef.join(" ")),
            color: 'rgba(255,0,0,0.8)'
        }, 
        leftL = { 
            vertices: Vertices.fromPath(leftLDef.join(" ")),
            color: 'rgba(0,0,255,0.8)'
        }, 
        rightL = { 
            vertices: Vertices.fromPath(rightLDef.join(" ")),
            color: 'rgba(0,255,0,0.8)'
        }, 
        leftS = { 
            vertices: Vertices.fromPath(leftSDef.join(" ")),
            color: 'rgba(255,255,0,0.8)'
        }, 
        rightS = { 
            vertices: Vertices.fromPath(rightSDef.join(" ")),
            color: 'rgba(0,255,255,0.8)'
        }, 
        square = { 
            vertices: Vertices.fromPath(squareDef.join(" ")),
            color: 'rgba(255,0,255,0.8)'
        };

    let blockOptions = [square,line,tee,leftL,rightL,leftS,rightS];

    // create engine
    let engine = Engine.create(),
        world = engine.world;

    // create renderer
    let render = Render.create({
        element: document.body,
        engine: engine,
        options: {
            width: windowWidth,
            height: windowHeight,
            wireframes: false
        }
    });

    Render.run(render);

    // create runner
    let runner = Runner.create();
    Runner.run(runner, engine);

    let leftKey = false,
        rightKey = false,
        downKey = false,
        dropKey = false,
        clockwiseRotateKey = false,
        antiClockwiseRotateKey = false;

    // piece movement
    let xVelocity = 0,
        yVelocity = 1,
        rotation = 0;

    let level = 1,
        rowsCleared = 0,
        gameOver = true;

    let currentBlock, 
        checkerCountdown = 60;

    Events.on(engine, 'beforeUpdate', function(event) {
        if (gameOver) {
            return;
        }

        if (checkerCountdown <= 0) {
            checkLines();
            checkerCountdown = 60;
        } else {
            checkerCountdown--;
        }

        if (currentBlock && currentBlock.position.y > 0) {
            
            if (leftKey) {
                xVelocity -= 0.1;
            } else if (rightKey) {
                xVelocity += 0.1;
            } else {
                xVelocity *= 0.2;
            }

            if (downKey) {
                yVelocity += 0.1;
            } else {
                yVelocity = level + (yVelocity * 0.2);
            }

            if (clockwiseRotateKey) {
                rotation += 0.005;
            } else if (antiClockwiseRotateKey) {
                rotation -= 0.005;
            } else {
                rotation *= 0.2;
            }

            if (dropKey) {
                releaseCurrentBlock();
            }
        }

        // remove any pieces that are beyond the window edges
        for(var block of world.bodies) {
            if (block.label === 'wall') continue;

            if (block.position.y > windowHeight 
                || block.position.x < 0 
                || block.position.x > windowWidth) {
                    World.remove(world, block);
                }
        }

        if (!currentBlock) {
            checkLines();  

            currentBlock = createBlock();
            currentBlock.parts.forEach(x => x.label = 'current');

            xVelocity = 0;
            yVelocity = level;
            rotation = 0;
        }

        Body.setVelocity(currentBlock, { x: xVelocity, y: yVelocity });
        Body.setAngularVelocity(currentBlock, rotation);
        Body.setPosition(currentBlock, { x: currentBlock.position.x + xVelocity * velocityModifier, y: currentBlock.position.y + yVelocity * velocityModifier });
    });


    Events.on(render, "afterRender", function(event) {
        let ctx = render.context,
            canvas = render.canvas;
        if (!gameOver) {            
            ctx.font = `${Math.floor(blockThickness * 2)}px Courier New`;
            ctx.fillStyle = "rgba(255,255,255,0.5)";
            ctx.textAlign = "center";
            ctx.fillText(rowsCleared, gameLeft + gameWidth/2,blockThickness * 2.5);
        }
    });

    function createBlock() {
        let options = blockOptions[Math.floor(Math.random() * blockOptions.length)],
            block = Bodies.fromVertices(
                gameLeft + ((gameWidth - (4 * blockThickness)) * Math.random()) + (2 * blockThickness),
                gameTop + blockThickness * -10,
                options.vertices,
                {   
                    render: {
                        fillStyle: options.color,
                        strokeStyle: options.color,
                        lineWidth: 1
                    },
                    label: 'block'
                });

        Body.rotate(block, Math.floor(Math.random() * 4) * 90 * Math.PI / 180);

        World.add(world, [block]);

        return block;
    }

    function releaseCurrentBlock() {
        if (!currentBlock) return;

        currentBlock.parts.forEach(x => x.label = 'block');
        currentBlock = null;
    }

    function checkLines() {
        let clearRowThreshold = 0.94 * gameWidth / boundSize,
            consecutiveRowsRequiredToClear = 10, 
            allBlocks = Composite.allBodies(engine.world).filter(x => x.label == 'block');

        let consecutiveRows = 0,
            rowBlocks = [];
        
        // loop from bottom
        for (let rowY = gameBottom; rowY >= 0; rowY -= boundSize) {
            let rowTotal = 0;            
            // loop from right
            for (let rowX = gameRight; rowX >= gameLeft; rowX -= boundSize) {
                let contents = Query.point(allBlocks, {x: rowX - boundSize/2, y: rowY-boundSize/2});
                if (contents.length) {
                    rowTotal++;
                    rowBlocks = rowBlocks.concat(contents);
                }
            }

            if (rowTotal > clearRowThreshold) {
                consecutiveRows++;
                if (consecutiveRows >= consecutiveRowsRequiredToClear) {
                    removeRow(Array.from(new Set(rowBlocks)), rowY-boundSize, rowY-boundSize + blockThickness);
                    consecutiveRows = 0;
                    rowBlocks = [];
                }
            } else {
                consecutiveRows = 0;
                rowBlocks = [];
            }

            //updateRowIndicator(rowY, rowTotal / clearRowThreshold);
        }
    }

    function removeRow(blocks, topBounds, bottomBounds) {

        // pause the game
        runner.enabled = false;

        // helper functions
        let isAboveBounds = (x) => x <= topBounds,
            isBelowBounds = (x) => x >= bottomBounds,
            isOutBounds = (x) => isAboveBounds(x) || isBelowBounds(x),
            isInBounds = (x) => !isOutBounds(x),
            createVertAtBounds = (vBelow, vAbove, bounds) => { 
                var distY = vBelow.y - vAbove.y,
                    distX = vBelow.x - vAbove.x,
                    distBounds = bounds - vAbove.y,
                    ratio = distBounds / distY,
                    x = distX * ratio + vAbove.x;
                return Vector.create(x, bounds);
            };
        
        let newBlocks = [];

        // so shit, but matterJS doesn't keep track of the vertices of concave objects
        let blockParts = []; 
        for (let block of blocks) {
            if (block.parts.length > 1) {
                blockParts.push.apply(blockParts, block.parts.slice(1));
            } else {
                blockParts.push(block);
            }
        }

        for(let blockIndex = 0; blockIndex < blockParts.length; blockIndex++) {
            let block = blockParts[blockIndex],
                newPaths = [],
                verts = block.vertices.map(x => Vector.clone(x));

            for (let i = 0; i < verts.length; i++) {
                let prev = verts[(verts.length + i -1) % verts.length],
                    curr = verts[i],
                    next = verts[(i+1) % verts.length];

                let path = newPaths.find(x => x.includes(prev));
                if (!path) newPaths.push(path = []);

                if (isInBounds(curr.y)) {
                    if (isAboveBounds(prev.y)) {
                        path.push(createVertAtBounds(curr, prev, topBounds));
                    } else if (isBelowBounds(prev.y)) {
                        path.push(createVertAtBounds(prev, curr, bottomBounds));
                    }

                    continue;
                }

                if (isAboveBounds(curr.y)) {
                    if (!isAboveBounds(prev.y)) {
                        if (isBelowBounds(prev.y)) {
                            let prevPath = newPaths.find(x => x.includes(prev));
                            if (!prevPath) newPaths.splice(newPaths.indexOf(path), 0, prevPath = []);

                            prevPath.push(createVertAtBounds(prev, curr, bottomBounds));
                        }

                        newPaths.push(path = []);
                        path.push(createVertAtBounds(prev, curr, topBounds));
                    }

                    path.push(curr);
                    continue;
                }

                if (isBelowBounds(curr.y)) {
                    if (!isBelowBounds(prev.y)) {
                        if (isAboveBounds(prev.y)) {
                            let prevPath = newPaths.find(x => x.includes(prev));
                            if (!prevPath) newPaths.splice(newPaths.indexOf(path), 0, prevPath = []);

                            prevPath.push(createVertAtBounds(curr, prev, topBounds));
                        }

                        newPaths.push(path = []);
                        path.push(createVertAtBounds(curr, prev, bottomBounds));
                    }

                    path.push(curr);
                    continue;
                }
            }

            let start = verts[0].y,
                end = [...verts].pop().y;

            if (isOutBounds(end) && newPaths.length > 1) {
                newPaths[0].push(...(newPaths.pop()));
            }

            // convert paths to blocks and add to newBlocks
            for(let newPath of newPaths) {
                if (Vertices.area(newPath) < (boundSize * boundSize)) continue;
                let centre = Vertices.centre(newPath),
                    newBlock = Bodies.fromVertices(
                        centre.x, 
                        centre.y, 
                        newPath, 
                        {
                            render: {
                                fillStyle: block.render.fillStyle,
                                strokeStyle: block.render.strokeStyle,
                                lineWidth: block.render.lineWidth
                            },
                            label: 'block',
                            oldParentId: block.parent.id
                        });
                
                newBlocks.push(newBlock);
            }
        }

        // combine composites
        let composites = [];
        for (var a=0; a < newBlocks.length; a++) {
            let blockA = newBlocks[a];

            for (let b=a+1; b < newBlocks.length; b++) {
                let blockB = newBlocks[b];

                if (blockA.oldParentId != blockB.oldParentId) continue;
                if (blockB.flaggedForRemoval && blockA.flaggedForRemoval) continue;

                let isComposite = false;
                for (let vA of blockA.vertices) {
                    for (let vB of blockB.vertices) {
                        // dodgy as fuck, check if rounded points are the same
                        if (Math.floor(vA.x) == Math.floor(vB.x) && Math.floor(vA.y) == Math.floor(vB.y)) {
                            isComposite = true;
                            break;
                        }
                    }
                    if (isComposite) break;
                }
                if (isComposite) {
                    let skipA, skipB;
                    let parts = composites.find(x => (skipA = x.includes(blockA)) || (skipB = x.includes(blockB)));
                    if (!parts) composites.push(parts = []);
                    if (!skipA) parts.push(blockA);
                    if (!skipB) parts.push(blockB);
                    blockA.flaggedForRemoval = true;
                    blockB.flaggedForRemoval = true;
                }
            }
        }

        if (composites.length) {
            newBlocks = newBlocks.filter(x => !x.flaggedForRemoval);
            
            for (let composite of composites) {
                newBlocks.push(Body.create({ parts: composite, label: 'block' }));
            }
        }

        World.remove(world, blocks);
        World.add(world, newBlocks);

        rowsCleared++;
        level = Math.floor(rowsCleared / 10) + 1;

        // resume the game
        runner.enabled = true;
        
    }

    function endGame() {
        gameOver = true;
        currentBlock = null;
        
        for (let body of Composite.allBodies(engine.world)) {
            Body.set(body, { isStatic: false, label: 'block', isSensor: false });
        }

        //var walls = Composite.allBodies(engine.world).filter(x => x.label === 'wall');
        //World.remove(world, walls);
    }

    function newGame() {
        //clear all
        World.remove(world, Composite.allBodies(engine.world));
        rowIndicators = [];

        // add indicators before walls
        // for (let rowY = gameBottom; rowY >= 0; rowY -= boundSize) {
        //     updateRowIndicator(rowY, 0.5);
        // }

        // add walls
        World.add(world, [
            // bottom 
            Bodies.rectangle(
                gameLeft + gameWidth / 2, 
                gameBottom + wallThickness / 2, 
                gameWidth, 
                wallThickness, 
                { 
                    isStatic: true, 
                    label: 'wall',
                    render: {
                        fillStyle: wallColor,
                        strokeStyle: wallColor,
                        lineWidth: 1
                    }
                }),
            // right
            Bodies.rectangle(
                gameRight + wallThickness / 2, 
                gameTop + gameHeight / 2, 
                wallThickness, 
                gameHeight, 
                { 
                    isStatic: true, 
                    label: 'wall',
                    render: {
                        fillStyle: wallColor,
                        strokeStyle: wallColor,
                        lineWidth: 1
                    } 
                }),
            // left
            Bodies.rectangle(
                gameLeft - wallThickness / 2, 
                gameTop + gameHeight / 2, 
                wallThickness, 
                gameHeight, 
                { 
                    isStatic: true, 
                    label: 'wall',
                    render: {
                        fillStyle: wallColor,
                        strokeStyle: wallColor,
                        lineWidth: 1
                    } 
                })
        ]);

        level = 1;
        rowsCleared = 0;
        gameOver = false;
        currentBlock = null;
    }

    let rowIndicators = {};
    function updateRowIndicator(rowY, percentageFull) {
        let indicator = rowIndicators[rowY];
        if (!indicator) {
            indicator = rowIndicators[rowY] = Bodies.rectangle(
            gameLeft + gameWidth / 2, 
            rowY - boundSize / 2, 
            gameWidth + wallThickness * 2,
            boundSize,
            {
                collisionFilter: {
                    mask: 0x0008
                },
                isSensor: true,
                isStatic: true,
                render: {
                    fillStyle: 'white',
                    lineWidth: 0
                },
                label: 'indicator'
            });
            World.add(world, indicator);
        }

        let rgb = Math.floor(percentageFull * 255),
            color = `rgb(${rgb},${rgb},${rgb})`;

        indicator.render.fillStyle = color;
    }

    Events.on(engine, 'collisionActive', function(event) {
        let i, pair,
            length = event.pairs.length;

        for (i = 0; i < length; i++) {
            pair = event.pairs[i];

            if (pair.bodyA.label === 'current' || pair.bodyB.label === 'current') {
                releaseCurrentBlock();
            }
            
            if (pair.bodyA.position.y < gameTop || pair.bodyB.position.y < gameTop) {
                endGame();
            }
        }
    });

    window.addEventListener('keydown', function(e) {
        switch (e.key) {
            case "a":
            case "ArrowLeft":
                leftKey = true;
                break;
            case "d":
            case "ArrowRight":
                rightKey = true;
                break;
            case "q":
                antiClockwiseRotateKey = true;
                break;
            case "e":
            case "w":
            case "ArrowUp":
                clockwiseRotateKey =  true;
                break;
            case "s":
            case "ArrowDown":
                downKey =  true;
                break;
            case "Control":
            case "Shift":
            case " ":
                dropKey = true;
                break;
            case "Enter": 
                if (!gameOver) break;
            case "F2":
                newGame();
                break;
            case "p":
                runner.enabled = runner.enabled ? false : true;
                break;
        }
    });

    window.addEventListener('keyup', function(e) {
        switch (e.key) {
            case "a":
            case "ArrowLeft":
                leftKey = false;
                break;
            case "d":
            case "ArrowRight":
                rightKey = false;
                break;
            case "q":
                antiClockwiseRotateKey = false;
                break;
            case "s":
            case "ArrowDown":
                downKey =  false;
                break;
            case "e":
            case "w":
            case "ArrowUp":
                clockwiseRotateKey = false;
                break;
            case "Control":
            case "Shift":
            case " ":
                dropKey = false;
                break;
        }
    });
})();